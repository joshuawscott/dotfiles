# Prerequistes

* tmux
* ruby-install
* chruby
* nvm
* reattach-to-user-namespace
* vim

* [asdf](https://github.com/asdf-vm/asdf):
```
git clone https://github.com/asdf-vm/asdf.git ~/.dotfiles/asdf
rcup
asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git

```

# Other setup
iterm2 Preferences -> Profiles -> Terminal -> Enable mouse reporting √
