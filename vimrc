let mapleader = ' '
filetype on
filetype off

call plug#begin('~/.vim/plugged')
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer --tern-completer' }
Plug 'kien/rainbow_parentheses.vim'
Plug 'pangloss/vim-javascript'
Plug 'rust-lang/rust.vim'
Plug 'guns/vim-clojure-static'
Plug 'fatih/vim-go'
Plug 'elzr/vim-json'
Plug 'vim-ruby/vim-ruby'
Plug 'justinmk/vim-syntax-extra'
Plug 'jimenezrick/vimerl'
Plug 'plasticboy/vim-markdown'
Plug 'jimmyharris/groovyindent'
Plug 'posva/vim-vue'

Plug 'neomake/neomake', { 'commit': '115508f53c44a1aa4a7623e0223a792c8b02cecc' }
Plug 'godlygeek/tabular'
Plug 'slashmili/alchemist.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'elixir-editors/vim-elixir'
Plug 'mhinz/vim-mix-format'

Plug 'b4b4r07/vim-hcl'

call plug#end()

syntax on
let &t_Co=256
filetype plugin indent on
"let macvim_skip_colorscheme = 1
set number
set relativenumber
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set showcmd
set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c\ \|\ %l/%L\ %P
set laststatus=2
set runtimepath^=~/.vim/bundle/ctrlp.vim
highlight MatchParen cterm=none ctermbg=yellow ctermfg=blue
colorscheme elflord
set exrc
set secure
set mouse=a

" Turn off buggy markdown folding
let g:vim_markdown_folding_disabled = 1

" Highlight mistakes (tabs, extra whitespace, maximum width)
highlight ExtraWhitespace ctermbg=red guibg=red
highlight SpecialKey ctermfg=1
set list
set listchars=tab:••
autocmd BufNewFile,BufRead *.go set nolist
match ExtraWhitespace /\s\+$/
set colorcolumn=121
highlight ColorColumn ctermbg=7

autocmd FileType c,cpp setlocal softtabstop=4
autocmd FileType c,cpp setlocal shiftwidth=4
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

autocmd BufRead,BufNewFile Jenkinsfile set syntax=groovy

" Ctrlp settings
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others']

" Find definitions and such
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
end

nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>
map <Leader>K :grep! "defp? \b<C-R><C-W>\b"<CR>:cw<CR>
map <Leader>o :cwindow<CR>
map <Leader>c :cclose<CR>
map <Leader>n :cnext<CR>
map <Leader>N :cprevious<CR>

" Neomake linters and such
autocmd! BufWritePost * Neomake
autocmd! BufEnter * Neomake
let g:neomake_elixir_enabled_makers = ['mix', 'credo']
let g:mix_format_on_save = 1
set statusline+=%#warningmsg#
set statusline+=%*

let mapleader = ','

set backspace=indent,eol,start
